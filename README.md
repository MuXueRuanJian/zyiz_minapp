# 找一找教程网-随时随地学软件编程
找一找教程网为IT专业技术人员提供专业的软件编程技术教程和最新的博客文章，是程序员自学软件编程的好帮手！
本站分为系列教程和技术博客文章；包括Java编程教程,python编程教程,asp.net core编程教程，c#编程教程，区块链编程，前端编程，后端，小程序编程，app编程，人工智能等编程技术文章。

#### 介绍
找一找教程网小程序源码，兼容微信小程序和百度小程序。效果查看--微信搜索“找一找教程网”。

#### 找一找教程网官网： [http://www.zyiz.net/](http://www.zyiz.net/)

#### 软件架构
使用uniapp开发的小程序。已经精心设计，完美兼容微信小程序和百度小程序。现在贡献出来，让需要的朋友可以快速搭建技术博客类的小程序，也可以让大家参考一番。若有好的想法，大家可以共同探讨。
####  效果图如下：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0322/231433_379f41ac_72497.jpeg "14.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0322/231441_cbc2363a_72497.jpeg "23.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0322/231450_334713d4_72497.jpeg "32.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0322/231455_69ff1044_72497.jpeg "41.jpg")

#### 安装教程

1.  使用最新版本的HBuilder X打开项目。
2.  本地安装微信小程序开发工具；
3.  HBuilder X编译，在微信小程序开发工具里查看效果；
4.  HBuilder X编译，在百度小程序开发工具里查看效果；
 
 #### 讨论
若有问题请留言。
 

 
